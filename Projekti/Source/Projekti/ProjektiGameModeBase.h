// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjektiGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJEKTI_API AProjektiGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
