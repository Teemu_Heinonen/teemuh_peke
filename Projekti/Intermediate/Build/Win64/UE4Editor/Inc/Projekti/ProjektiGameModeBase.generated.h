// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJEKTI_ProjektiGameModeBase_generated_h
#error "ProjektiGameModeBase.generated.h already included, missing '#pragma once' in ProjektiGameModeBase.h"
#endif
#define PROJEKTI_ProjektiGameModeBase_generated_h

#define Projekti_Source_Projekti_ProjektiGameModeBase_h_15_SPARSE_DATA
#define Projekti_Source_Projekti_ProjektiGameModeBase_h_15_RPC_WRAPPERS
#define Projekti_Source_Projekti_ProjektiGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Projekti_Source_Projekti_ProjektiGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjektiGameModeBase(); \
	friend struct Z_Construct_UClass_AProjektiGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AProjektiGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Projekti"), NO_API) \
	DECLARE_SERIALIZER(AProjektiGameModeBase)


#define Projekti_Source_Projekti_ProjektiGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAProjektiGameModeBase(); \
	friend struct Z_Construct_UClass_AProjektiGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AProjektiGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Projekti"), NO_API) \
	DECLARE_SERIALIZER(AProjektiGameModeBase)


#define Projekti_Source_Projekti_ProjektiGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjektiGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjektiGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjektiGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjektiGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjektiGameModeBase(AProjektiGameModeBase&&); \
	NO_API AProjektiGameModeBase(const AProjektiGameModeBase&); \
public:


#define Projekti_Source_Projekti_ProjektiGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjektiGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjektiGameModeBase(AProjektiGameModeBase&&); \
	NO_API AProjektiGameModeBase(const AProjektiGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjektiGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjektiGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjektiGameModeBase)


#define Projekti_Source_Projekti_ProjektiGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Projekti_Source_Projekti_ProjektiGameModeBase_h_12_PROLOG
#define Projekti_Source_Projekti_ProjektiGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Projekti_Source_Projekti_ProjektiGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Projekti_Source_Projekti_ProjektiGameModeBase_h_15_SPARSE_DATA \
	Projekti_Source_Projekti_ProjektiGameModeBase_h_15_RPC_WRAPPERS \
	Projekti_Source_Projekti_ProjektiGameModeBase_h_15_INCLASS \
	Projekti_Source_Projekti_ProjektiGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Projekti_Source_Projekti_ProjektiGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Projekti_Source_Projekti_ProjektiGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Projekti_Source_Projekti_ProjektiGameModeBase_h_15_SPARSE_DATA \
	Projekti_Source_Projekti_ProjektiGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Projekti_Source_Projekti_ProjektiGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Projekti_Source_Projekti_ProjektiGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJEKTI_API UClass* StaticClass<class AProjektiGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Projekti_Source_Projekti_ProjektiGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
